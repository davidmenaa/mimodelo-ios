
import UIKit
import CoreData

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if let miDelegate = UIApplication.shared.delegate as? AppDelegate {
            let miContexto = miDelegate.persistentContainer.viewContext
            let u = NSEntityDescription.insertNewObject(forEntityName: "Usuario", into: miContexto) as! Usuario
            u.login = "Pepe"
            u.password = "123456"
            try! miContexto.save()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
