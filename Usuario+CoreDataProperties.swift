//
//  Usuario+CoreDataProperties.swift
//  MiModelo
//
//  Created by David Mena on 25/2/17.
//  Copyright © 2017 David Mena. All rights reserved.
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension Usuario {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Usuario> {
        return NSFetchRequest<Usuario>(entityName: "Usuario");
    }

    @NSManaged public var creditos: Int32
    @NSManaged public var login: String?
    @NSManaged public var password: String?
    @NSManaged public var conversaciones: NSSet?
    @NSManaged public var mensajes: NSSet?

}

// MARK: Generated accessors for conversaciones
extension Usuario {

    @objc(addConversacionesObject:)
    @NSManaged public func addToConversaciones(_ value: Conversacion)

    @objc(removeConversacionesObject:)
    @NSManaged public func removeFromConversaciones(_ value: Conversacion)

    @objc(addConversaciones:)
    @NSManaged public func addToConversaciones(_ values: NSSet)

    @objc(removeConversaciones:)
    @NSManaged public func removeFromConversaciones(_ values: NSSet)

}

// MARK: Generated accessors for mensajes
extension Usuario {

    @objc(addMensajesObject:)
    @NSManaged public func addToMensajes(_ value: Mensaje)

    @objc(removeMensajesObject:)
    @NSManaged public func removeFromMensajes(_ value: Mensaje)

    @objc(addMensajes:)
    @NSManaged public func addToMensajes(_ values: NSSet)

    @objc(removeMensajes:)
    @NSManaged public func removeFromMensajes(_ values: NSSet)

}
