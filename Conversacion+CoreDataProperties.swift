//
//  Conversacion+CoreDataProperties.swift
//  MiModelo
//
//  Created by David Mena on 25/2/17.
//  Copyright © 2017 David Mena. All rights reserved.
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension Conversacion {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Conversacion> {
        return NSFetchRequest<Conversacion>(entityName: "Conversacion");
    }

    @NSManaged public var comienzo: NSDate?
    @NSManaged public var mensajes: NSSet?
    @NSManaged public var usuarios: NSSet?

}

// MARK: Generated accessors for mensajes
extension Conversacion {

    @objc(addMensajesObject:)
    @NSManaged public func addToMensajes(_ value: Mensaje)

    @objc(removeMensajesObject:)
    @NSManaged public func removeFromMensajes(_ value: Mensaje)

    @objc(addMensajes:)
    @NSManaged public func addToMensajes(_ values: NSSet)

    @objc(removeMensajes:)
    @NSManaged public func removeFromMensajes(_ values: NSSet)

}

// MARK: Generated accessors for usuarios
extension Conversacion {

    @objc(addUsuariosObject:)
    @NSManaged public func addToUsuarios(_ value: Usuario)

    @objc(removeUsuariosObject:)
    @NSManaged public func removeFromUsuarios(_ value: Usuario)

    @objc(addUsuarios:)
    @NSManaged public func addToUsuarios(_ values: NSSet)

    @objc(removeUsuarios:)
    @NSManaged public func removeFromUsuarios(_ values: NSSet)

}
