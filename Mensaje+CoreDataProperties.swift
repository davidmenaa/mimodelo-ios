//
//  Mensaje+CoreDataProperties.swift
//  MiModelo
//
//  Created by David Mena on 25/2/17.
//  Copyright © 2017 David Mena. All rights reserved.
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension Mensaje {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Mensaje> {
        return NSFetchRequest<Mensaje>(entityName: "Mensaje");
    }

    @NSManaged public var fecha: NSDate?
    @NSManaged public var texto: String?
    @NSManaged public var conversacion: Conversacion?
    @NSManaged public var usuario: Usuario?

}
